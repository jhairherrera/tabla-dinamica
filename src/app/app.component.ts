import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tabla-dinámica';
  columns!: i_columns[];
  rows: i_rows[] = [];

  constructor(
    private httpService: HttpClient,
  ) {
    this.columns = [
      {
        id: "userId", // igual al key_name del parametro de los datos
        placeholder: "Nombre de usuario",
        show: true,
        width: "200px",
      },
      {
        id: "id", // igual al key_name del parametro de los datos
        placeholder: "Identificador",
        show: true,
        width: "100px",
      },
      {
        id: "title", // igual al key_name del parametro de los datos
        placeholder: "Nombre",
        show: true,
        width: "500px",
      }
      , {
        id: "completed", // igual al key_name del parametro de los datos
        placeholder: "¿Está completo?",
        show: true,
        width: "120px",
      }
    ];
    this.formatData();
  }

  ngOnInit(): void {
  }

  formatData() {
    this.getData().subscribe({
      next: (response: any) => {
        if (response) {
          if (response.length > 0) {
            for (let a = 0; a < response.length; a++) {
              const item = response[a];
              this.rows.push({
                userId: item.userId ?? 0,
                id: item.id ?? 0,
                title: item.title ?? "",
                completed: item.completed ? "Si" : "No",
                block: false,
                show: true,
                select: false
              })
            }
            console.log(this.rows);
          }
        }
      },
      error: (error: any) => {
        console.log(error);
      },
    })
  }

  getData(): Observable<any> {
    return this.httpService.get<any>("https://jsonplaceholder.typicode.com/todos");
  }
}

export interface i_columns {
  id: string;
  placeholder: string;
  show: boolean;
  width: string;
}

export interface i_rows {
  userId: number;
  id: number;
  title: string;
  completed: string;

  block: boolean;
  show: boolean;
  select: boolean;
}
