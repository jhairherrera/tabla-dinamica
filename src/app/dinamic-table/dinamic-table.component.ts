import { Component, Input, OnInit } from '@angular/core';
import { i_columns } from '../app.component';

@Component({
  selector: 'app-dinamic-table',
  templateUrl: './dinamic-table.component.html',
  styleUrls: ['./dinamic-table.component.css']
})
export class DinamicTableComponent implements OnInit {
  @Input() columns: i_columns[] = [];
  @Input() rows: any[] = [];
  @Input() with_actions: boolean = true;
  @Input() with_filters: boolean = true;
  @Input() with_pagination: boolean = true;
  page: number = 1;
  configPaginator = { id: 'custom', itemsPerPage: 10, currentPage: this.page }
  orderKey!: string;
  reverse: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

}
